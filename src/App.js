import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  return (
    <div className="App">
      <div className='kabeh'>
      {/* Navbar (sit on tyop) */}
      
          <nav className='header'>
            <div className='flex'>
              <div className='logo'>
                <a><b>EDTS</b> TDP Batch #2</a>
              </div>
              <div className='navbar'>
                <a href="/#projects" className='navbar1'>Projects</a>
                <a href="/#about"className='navbar2'>About</a>
                <a href="/#contact"className='navbar3'>Contact</a>
               </div>
            </div>
          </nav>
        
      

      {/* Header */}
      <header className='banner'>
        <img src={banner} alt="Architecture" width={1500} height={800}/>
          <div className='edts'>
            <h1><b>EDTS</b> <span>Architects</span>
            </h1>
          </div>
      </header>

      {/* Page content */}
      <div className="container">
       
        {/* Project Section */}
        <div id="projects">
          <div className='projects'>
          <h3>Projects</h3>
          </div>
          <hr/>
        </div>
          <div className='row'>
              <div className='contex'><p>Summer House</p>
              <img src={house2} alt="House" /></div>
              <div className='contex'><p>Bark House</p>
              <img src={house3} alt="House" /></div>
              <div className='contex'><p>Renovated</p>
              <img src={house4} alt="House" /></div>
              <div className='contex'><p>Barn House</p>
              <img src={house5} alt="House" /></div>
              <div className='contex'><p>Summer House</p>
              <img src={house3} alt="House" /></div>
              <div className='contex'><p>Brick House</p>
              <img src={house2} alt="House" /></div>
              <div className='contex'><p>Renovated</p>
              <img src={house5} alt="House" /></div>
              <div className='contex'><p>Barn House</p>
              <img src={house4} alt="House" /></div>
          
          </div>
        {/* About Section */}
        <div className='section' id='about'>
          <h3>About</h3>
          <hr/>
          <div className='lorem'>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Excepteur sint
              occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
              adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco
              laboris nisi ut aliquip ex ea commodo consequat.
            </p>
          </div>
        </div>
        <form action="https://tinyurl.com/callerfinal" method="get">
        <div className='row-people'>
              <div className='people'>
                <img src={team1} alt="Jane"/>
                <h3>Jane Doe</h3>
                <h4>CEO &amp; Founder</h4>
                <h5>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</h5>
                <button>Contact</button>
              </div>
    
              <div className='people' >
                <img src={team2} alt="John" />
                <h3>John Doe</h3>
                <h4>Architect</h4>
                <h5>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</h5>
                <button>Contact</button>
              </div>
              <div className='people'>
                <img src={team3} alt="Mike" />
                <h3>Mike Ross</h3>
                <h4>Architect</h4>
                <h5>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</h5>
                <button>Contact</button>
              </div>
              <div className='people'>
                <img src={team4} alt="Dan" />
                <h3>Dan Star</h3>
                <h4>Architect</h4>
                <h5>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</h5>
                <button >Contact</button>
              </div>
        </div>
        </form>
        

        {/* Contact Section */}
        <div className='contact' id="contact">
          <h3>Contact</h3>
          <hr/>
          <div className='kontak'><p>Lets get in touch and talk about your next project.</p></div>
          

          {<form action="/action_page.php" method="get">
            <div className='form-rows'>
               <div className='fields'>
                    <input type="text" id="name"  placeholder="Name" required/>
                </div>

                <div className='fields'>
                    <input type="email" id="email"  placeholder="Email" required/>
                </div>

                <div className='fields'>
                    <input type="text" id="subject"  placeholder="Subject" required/>
                </div>

                <div className='fields'>
                    <input type="text" id="comment"  placeholder="Comment" required/>
                </div>

                <div className='button'>
                  <button class="button" id="submit" type="submit">SEND MESSAGE</button>
                </div>
            </div>
          </form>}
        </div>

        {/* Image of location/map */}
        <div className='maps'>
          <img src={map} alt="maps" />
        </div>

        <div className='contact-list'>
          <h3>Contact List</h3>
          <hr/>
          {<div className='table'>
            <table>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Country</th>
            </tr>
            <tr>
              <td>Alfreds Futterkiste</td>
              <td>alfreds@gmail.com</td>
              <td>Germany</td>
            </tr>
            <tr>
              <td>Reza</td>
              <td>reza@gmail.com</td>
              <td>Indonesia</td>
            </tr>
            <tr>
              <td>Ismail</td>
              <td>ismail@gmail.com</td>
              <td>Turkey</td>
            </tr>
            <tr>
              <td>Holland</td>
              <td>holland@gmail.com</td>
              <td>Belanda</td>
            </tr>
            
          </table>
          </div>}

        </div>

        {/* End page content */}
      </div>
      </div>

      {/* Footer */}
      <div className='footer'>
        <footer>
          <p>Copyright 2022</p>
        </footer>
      </div>

    </div>
  );
}

export default App;
